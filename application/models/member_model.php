<?php
	class Member_model extends CI_Model{
		function __construct()
		{
			// 呼叫模型(Model)的建構函數
			parent::__construct();
    	}
		function member_intro($id)
		{
			$this->db->select('introduction, ext')->from('exp_pre_lab_member')->where('id', $id);
			$query = $this->db->get();
			return $query;
		}
		function member_name()
		{
			$this->db->select('name, grade, ext')->from('exp_pre_lab_member')->where('grade <= 3');
			$query = $this->db->get();
			return $query;
		}
		function member_history()
		{
			$this->db->from('exp_pre_lab_member')->where('grade > 3')->order_by("year", "desc");
			$query = $this->db->get();
			return $query;
		}
	}
?>