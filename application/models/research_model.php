<?php
	class Research_model extends CI_Model{
		function __construct()
		{
			// 呼叫模型(Model)的建構函數
			parent::__construct();
    	}
		function project()
		{
			$this->db->order_by("sql_id", "desc");
			$query = $this->db->get('exp_pre_project');

			return $query;
		}
		function plan()
		{
			$query = $this->db->get('exp_pre_plan');

			return $query;
		}
		function paper()
		{
			$query = $this->db->get('exp_pre_lab_member');

			return $query;
		}
		
	}
?>