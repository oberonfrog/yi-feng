<?php
	class Entry_model extends CI_Model{
		function __construct()
		{
			// 呼叫模型(Model)的建構函數
			parent::__construct();
    	}
		function intro(){
			$query = $this->db->get('exp_pre_lab_introduction');
			return $query;
		}
		function news(){
			$this->db->order_by("evt_time", "desc");
			$query = $this->db->get('exp_pre_latest_news');
			return $query;
		}
	}
?>