<?php
	class Paper_model extends CI_Model{
		function __construct()
		{
			// 呼叫模型(Model)的建構函數
			parent::__construct();
    	}
		function recent_research(){
			$query = $this->db->get('exp_pre_recent_research');
			return $query;
		}
		function paper(){
			$query = $this->db->get('exp_pre_professor_paper');
			return $query;
		}
	}
?>