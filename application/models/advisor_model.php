<?php
	class Advisor_model extends CI_Model{
		function __construct()
		{
			// 呼叫模型(Model)的建構函數
			parent::__construct();
    	}
		function exp(){
			$this->db->select('institution, department, job, DATE_FORMAT(start_date, "%Y-%m") AS start_date, DATE_FORMAT(end_date, "%Y-%m") AS end_date', FALSE);
			$this->db->order_by("start_date", "asc");
			$query = $this->db->get('exp_pre_professor_exp');
			return $query;
		}
		function speciality(){
			$query = $this->db->get('exp_pre_professor_speciality');
			return $query;
		}
	}
?>