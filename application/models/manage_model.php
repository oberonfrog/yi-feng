<?php
	class Manage_model extends CI_Model{
		function __construct()
		{
			// 呼叫模型(Model)的建構函數
			parent::__construct();
    	}
		function get_news()
		{
			$this->db->order_by("evt_time", "desc");
			$query = $this->db->get('exp_pre_latest_news');
			return $query;
		}
		function get_intro()
		{
			$query = $this->db->get('exp_pre_lab_introduction');
			return $query;
		}
		function get_exp()
		{
			$this->db->select('institution, department, job, sql_id,  DATE_FORMAT(start_date, "%Y-%m") AS start_date, DATE_FORMAT(end_date, "%Y-%m") AS end_date', FALSE);
			$this->db->order_by("start_date", "asc");
			$query = $this->db->get('exp_pre_professor_exp');
			return $query;
		}
		function get_speciality()
		{
			$query = $this->db->get('exp_pre_professor_speciality');
			return $query;
		}
		function get_professor_paper()
		{
			$query = $this->db->get('exp_pre_professor_paper');
			return $query;
		}
		
		function get_project()
		{
			$query = $this->db->get('exp_pre_project');

			return $query;
		}
		function get_plan()
		{
			$query = $this->db->get('exp_pre_plan');

			return $query;
		}
		function get_paper()
		{
			$query = $this->db->get('exp_pre_lab_member');
			return $query;
		}
		
		function get_member()
		{
			$this->db->select('name,grade,introduction,ext,sql_id')->from('exp_pre_lab_member')->where('grade <= 3');
			$this->db->order_by("grade", "asc");
			$query = $this->db->get();
			return $query;	
		}
		
		function get_picture()
		{
			$query = $this->db->get('exp_pre_picture');
			return $query;
		}
		function get_old_member()
		{
			$this->db->select('name, paper_name, paper_text, sql_id')->from('exp_pre_lab_member')->where('grade > 3');
			$query = $this->db->get();
			return $query;
		}
	}
?>