<?php
	class Edit_model extends CI_Model{
		function __construct()
		{
			// 呼叫模型(Model)的建構函數
			parent::__construct();
    	}
		function news_add()
		{
			$content = $this->input->post('content');
			$date = $this->input->post('date');
			$this->db->set('notice', $content);
			$this->db->set('evt_time', $date);
			$this->db->insert('exp_pre_latest_news');
			$this->db->select_max('sql_id');
			$sql_id = $this->db->get('exp_pre_latest_news')->result()[0]->sql_id;
			return $sql_id;
		}
		function news_modify()
		{
			$content = $this->input->post('content');
			$date = $this->input->post('date');
			$sql_id = $this->input->post('sql_id');
			$this->db->set('notice', $content);
			$this->db->set('evt_time', $date);
			$this->db->where('sql_id',$sql_id)->update('exp_pre_latest_news'); 
		}
		function news_delete()
		{
			$sql_id = $this->input->post('sql_id');
			$this->db->where('sql_id', $sql_id);
			$this->db->delete('exp_pre_latest_news'); 
		}
		function intro_modify()
		{
			$content = trim($this->input->post('content'));
			$this->db->truncate('exp_pre_lab_introduction');
			$this->db->set('introduction', $content);
			$this->db->insert('exp_pre_lab_introduction');
		}
		
		function exp_add()
		{
			$institution = trim($this->input->post('institution'));
			$department = trim($this->input->post('department'));
			$job = trim($this->input->post('job'));
			$start_date = trim($this->input->post('start_date'));
			$end_date = trim($this->input->post('end_date'));
			$this->db->set('institution', $institution);
			$this->db->set('department', $department);
			$this->db->set('job', $job);
			$this->db->set('start_date', $start_date);
			$this->db->set('end_date', $end_date);
			$this->db->insert('exp_pre_professor_exp');
			$this->db->select_max('sql_id');
			$sql_id = $this->db->get('exp_pre_professor_exp')->result()[0]->sql_id;
			return $sql_id;
		}
		function exp_modify()
		{
			$institution = trim($this->input->post('institution'));
			$department = trim($this->input->post('department'));
			$job = trim($this->input->post('job'));
			$start_date = trim($this->input->post('start_date'));
			$end_date = trim($this->input->post('end_date'));
			$sql_id = trim($this->input->post('sql_id'));
			$this->db->set('institution', $institution);
			$this->db->set('department', $department);
			$this->db->set('job', $job);
			$this->db->set('start_date', $start_date);
			$this->db->set('end_date', $end_date);
			$this->db->where('sql_id',$sql_id)->update('exp_pre_professor_exp');
		}
		function exp_delete()
		{
			$sql_id = $this->input->post('sql_id');
			$this->db->where('sql_id', $sql_id);
			$this->db->delete('exp_pre_professor_exp'); 
		}
		
		function speciality_add()
		{
			$speciality = trim($this->input->post('speciality'));
			$this->db->set('speciality', $speciality);
			$this->db->insert('exp_pre_professor_speciality');
			$this->db->select_max('sql_id');
			$sql_id = $this->db->get('exp_pre_professor_speciality')->result()[0]->sql_id;
			return $sql_id;
		}
		function speciality_modify()
		{
			$speciality = trim($this->input->post('speciality'));
			$sql_id = trim($this->input->post('sql_id'));
			$this->db->set('speciality', $speciality);
			$this->db->where('sql_id',$sql_id)->update('exp_pre_professor_speciality');
		}
		function speciality_delete()
		{
			$sql_id = $this->input->post('sql_id');
			$this->db->where('sql_id', $sql_id);
			$this->db->delete('exp_pre_professor_speciality'); 
		}
		
		function paper_add()
		{
			$paper = trim($this->input->post('paper'));
			$this->db->set('paper', $paper);
			$this->db->insert('exp_pre_professor_paper');
			$this->db->select_max('sql_id');
			$sql_id = $this->db->get('exp_pre_professor_paper')->result()[0]->sql_id;
			return $sql_id;
		}
		function paper_modify()
		{
			$paper = trim($this->input->post('paper'));
			$sql_id = trim($this->input->post('sql_id'));
			$this->db->set('paper', $paper);
			$this->db->where('sql_id',$sql_id)->update('exp_pre_professor_paper');
		}
		function paper_delete()
		{
			$sql_id = trim($this->input->post('sql_id'));
			$this->db->where('sql_id', $sql_id);
			$this->db->delete('exp_pre_professor_paper'); 
		}
		
		function research_add()
		{
			$name = trim($this->input->post('title'));
			$this->db->set('name', $name);
			$this->db->insert('exp_pre_project');
			$this->db->select_max('sql_id');
			$sql_id = $this->db->get('exp_pre_project')->result()[0]->sql_id;
			return $sql_id;
		}
		
		function research_modify()
		{
			$name = trim($this->input->post('title'));
			$introduction = trim($this->input->post('intro'));
			$sql_id = trim($this->input->post('sql_id'));
			$this->db->set('name', $name);
			$this->db->set('introduction', $introduction);
			$this->db->where('sql_id',$sql_id)->update('exp_pre_project');
		}
		
		function research_delete()
		{
			$sql_id = trim($this->input->post('sql_id'));
			$this->db->where('sql_id', $sql_id);
			$this->db->delete('exp_pre_project');
			$this->db->where('source', $sql_id);
			$this->db->delete('exp_pre_plan');
			$this->db->where('source', $sql_id);
			$this->db->delete('exp_pre_paper');
		}
		function relative_plan_add()
		{
			$name = trim($this->input->post('name'));
			$institution = trim($this->input->post('institution'));
			$start_date = trim($this->input->post('start_date'));
			$end_date = trim($this->input->post('end_date'));
			$source = trim($this->input->post('source'));
			
			$this->db->set('name', $name);
			$this->db->set('institution', $institution);
			$this->db->set('start_date', $start_date);
			$this->db->set('end_date', $end_date);
			$this->db->set('source', $source);
			$this->db->insert('exp_pre_plan');
			
			$this->db->select_max('sql_id');
			$sql_id = $this->db->get('exp_pre_plan')->result()[0]->sql_id;
			return $sql_id;
		}
		function relative_plan_modify()
		{
			$name = trim($this->input->post('name'));
			$institution = trim($this->input->post('institution'));
			$start_date = trim($this->input->post('start_date'));
			$end_date = trim($this->input->post('end_date'));
			$sql_id = trim($this->input->post('sql_id'));
			$this->db->set('name', $name);
			$this->db->set('institution', $institution);
			$this->db->set('start_date', $start_date);
			$this->db->set('end_date', $end_date);
			$this->db->where('sql_id',$sql_id)->update('exp_pre_plan');
		}
		
		function relative_plan_delete()
		{
			$sql_id = trim($this->input->post('sql_id'));
			$this->db->where('sql_id', $sql_id);
			$this->db->delete('exp_pre_plan'); 
		}
		
		function relative_paper_add()
		{
			$name = trim($this->input->post('name'));
			$source = trim($this->input->post('source'));
			
			$this->db->set('paper', $name);
			$this->db->set('source', $source);
			$this->db->insert('exp_pre_paper');
			
			$this->db->select_max('sql_id');
			$sql_id = $this->db->get('exp_pre_paper')->result()[0]->sql_id;
			return $sql_id;
		}
		function relative_paper_modify()
		{
			$name = trim($this->input->post('name'));
			$sql_id = trim($this->input->post('sql_id'));
			$this->db->set('paper', $name);
			$this->db->where('sql_id',$sql_id)->update('exp_pre_paper');
		}
		
		function relative_paper_delete()
		{
			$sql_id = trim($this->input->post('sql_id'));
			$this->db->where('sql_id', $sql_id);
			$this->db->delete('exp_pre_paper'); 
		}
		
		function picture_upload()
		{
			$file_name = trim($this->input->post('title'));
			$md5_file_name = md5($file_name);
			
			$config['upload_path'] = './static/img/picture';
			$config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG|jpeg|JPEG';
			$config['file_name'] = $md5_file_name;
			$this->load->library('upload',$config);
			$this->upload->initialize($config);
			$this->upload->do_upload('origin');
			$ext = $this->upload->data()['file_ext'];
			$config['upload_path'] = './static/img/picture/thumbnail';
			$this->upload->initialize($config);
			$this->upload->do_upload('short');
			$this->db->set('title', $file_name);
			$this->db->set('name', $md5_file_name.$ext);
			$this->db->insert('exp_pre_picture');
		}
		function picture_delete()
		{
			$sql_id = trim($this->input->post('sql_id'));
			$this->db->select('name')->from('exp_pre_picture')->where('sql_id', $sql_id);
			$name = $this->db->get()->result()[0]->name;
			$this->db->where('sql_id', $sql_id);
			$this->db->delete('exp_pre_picture');
			return $name;
		}
		
		function member_modify()
		{
			$sql_id = trim($this->input->post('sql_id'));
			$intro = nl2br($this->input->post('intro'));
			$grade = trim($this->input->post('grade'));
			$this->db->select('*')->from('exp_pre_lab_member')->where('sql_id', $sql_id);
			$name = $this->db->get()->result()[0]->name;
			$md5_file_name = md5($name);
			$config['upload_path'] = './static/img/member-pic';
			$config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG|jpeg|JPEG';
			$config['file_name'] = $md5_file_name;
			$config['overwrite'] = "true";
			$this->load->library('upload',$config);
			$this->upload->initialize($config);
			if($this->upload->do_upload('pic1')){
				$ext = $this->upload->data()['file_ext'];
			}
			
			$config['upload_path'] = './static/img/member-life';
			$this->upload->initialize($config);
			if($this->upload->do_upload('pic2')){
				$ext = $this->upload->data()['file_ext'];
			}
			
			$this->db->set('introduction', $intro);
			$this->db->set('grade', $grade);
			if(isset($ext) && !is_null($ext)){
				$this->db->set('ext', $ext);
			}
			$this->db->where('sql_id',$sql_id)->update('exp_pre_lab_member');
		}
		
		function member_add()
		{
			$name = trim($this->input->post('name'));
			$this->db->set('name', $name);
			$this->db->set('id', md5($name));
			$this->db->insert('exp_pre_lab_member');
		}
		
		function member_delete()
		{
			$sql_id = trim($this->input->post('sql_id'));
			$this->db->select('id, ext')->from('exp_pre_lab_member')->where('sql_id', $sql_id);
			$query = $this->db->get()->result()[0];
			$this->db->where('sql_id', $sql_id);
			$this->db->delete('exp_pre_lab_member');
			return $query;
		}
		
		function old_member_modify()
		{
			$sql_id = trim($this->input->post('sql_id'));
			$name = trim($this->input->post('name'));
			$author = trim($this->input->post('author'));
			$content = trim($this->input->post('content'));
			$age = trim($this->input->post('age'));
            $cat = trim($this->input->post('cat'));
            
			$this->db->set('paper_name', $name);
			$this->db->set('name', $author);
			$this->db->set('paper_text', $content);
            $this->db->set('paper_cat', $cat);
            $this->db->set('year', $age);
			$this->db->where('sql_id',$sql_id)->update('exp_pre_lab_member');
			return $content;
		}
		
		function old_member_add()
		{
			$author = trim($this->input->post('author'));
			$this->db->set('name', $author);
			$this->db->set('id', md5($author));
			$this->db->set('grade', 4);
			$this->db->insert('exp_pre_lab_member');
			$this->db->select_max('sql_id');
			$sql_id = $this->db->get('exp_pre_lab_member')->result()[0]->sql_id;
            
            $this->db->order_by("sql_id", "desc");
			$query = $this->db->get('exp_pre_project')->result();
			return array(
                "sql_id" => $sql_id,
                "project" => $query
            );
		}
		
		function old_member_delete()
		{
			$sql_id = trim($this->input->post('sql_id'));
			$this->db->where('sql_id', $sql_id);
			$this->db->delete('exp_pre_lab_member');
		}
	}
?>