<div class="container  main_page professor">
	<div class="row">
		<div class="col-sm-3 text-center">
			<img src="<?php echo base_url();?>static/img/professor.png" style="box-shadow: 0px 0px 2px #ccc;">
			<h4>
				洪一峯
			</h4>
			<h4>
				Yi-Feng Hung
			</h4>
		</div>
		<div class="col-sm-9 padded-bottom">
			<h4 class="content-title">學歷</h4>	
			<ul class="list">
				<li>1986-1991 博士 柏克萊加州大學 Industrial Engineering & Operations Research</li>
				<li>1986-1989 碩士 柏克萊加州大學 Industrial Engineering & Operations Research</li>
				<li>1980-1984 學士 國立清華大學 工業工程與工程管理學系</li>
			</ul>

			<h4 class="content-title">經歷</h4>
			<ul class="list">
				<?php foreach($exp->result() as $row):?>
				<li>
					<?php
						echo $row->institution." ".$row->department." ".$row->job." ".$row->start_date."~"; 
						if($row->end_date == "0000-00")
						{
							echo date("Y-m", time());
						}
						else
						{
							echo $row->end_date;
						}
					?>

				</li>
				<?php endforeach; ?>
			</ul>
			<h4 class="content-title">專長學科</h4>
			<ul class="list">
				<?php foreach($speciality->result() as $row):?>
				<li><?php echo $row->speciality;?></li>
				<?php endforeach;?>
			</ul>
            <h4 class="content-title">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h4>
		</div>
	</div>

</div>
