<div class="col-sm-9">
	<div class="row padded advisor_exp">
		<h4 class="content-title">經歷</h4>
		<table class="table table-bordered manage-table">
			<thead>
				<tr>
					<th>服務機關</th>
					<th>服務部門</th>
					<th>職稱</th>
					<th width="10%">開始時間</th>
					<th width="10%">結束時間</th>
					<th width="8%"></th>
					<th width="8%">
						<button type="button" class="add btn btn-primary btn-xs">新增</button>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($exp->result() as $row):?>
				<tr>
					<td class="institution">
						<?php echo $row->institution;?></td>
					<td class="department">
						<?php echo $row->department;?></td>
					<td class="job">
						<?php echo $row->job;?></td>
					<td class="start_date">
						<?php echo $row->start_date;?></td>
					<td class="end_date">
						<?php if($row->end_date == "0000-00")
								{
									echo date("Y-m", time());
								}
								else
								{
									echo $row->end_date;
								}
						?></td>
					<td class="handle">
						<button type="button" class="<?php if($row->end_date == "0000-00"){echo "now";}?> modify btn btn-info btn-xs" data-id="<?php echo $row->sql_id;?>">修改</button>
					</td>
					<td class="handle2">
						<button type="button" class="delete btn btn-danger btn-xs" data-id="<?php echo $row->sql_id;?>">刪除</button>
					</td>
				</tr>
				<?php endforeach;?>
			</tbody>
			<tfoot>
			</tfoot>
		</table>
	</div>
	<div class="row padded advisor_speciality">
		<h4 class="content-title">專長學科</h4>
		<table class="table table-bordered manage-table">
			<thead>
				<tr>
					<th>專長</th>
					<th width="8%"></th>
					<th width="8%">
						<button type="button" class="add btn btn-primary btn-xs">新增</button>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($speciality->result() as $row):?>
				<tr>
					<td class="speciality">
						<?php echo $row->speciality;?></td>
					<td class="handle">
						<button type="button" class="modify btn btn-info btn-xs" data-id="<?php echo $row->sql_id;?>">修改</button>
					</td>
					<td class="handle2">
						<button type="button" class="delete btn btn-danger btn-xs" data-id="<?php echo $row->sql_id;?>">刪除</button>
					</td>
				</tr>
				<?php endforeach;?>
			</tbody>
			<tfoot>
			</tfoot>
		</table>
	</div>
</div>
</div>
</div>