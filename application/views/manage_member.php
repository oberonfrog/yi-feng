<div class="col-sm-9">
	<div class="row padded lab-member">
		<h4 class="content-title">成員</h4>
		<div class="row padded">
			<div class="input-group add-member pull-right">
					<input type="text" id="member-name" width="40%">
					<button type="button" class="add btn btn-primary btn-xs">新增成員</button>
			</div>
		</div>
		<?php foreach($member->result() as $row):?>
		<form action="<?php echo site_url('index.php/edit/member/modify');?>" method="post" enctype="multipart/form-data" >
		<table class="table table-bordered manage-table">
				<tr>
					<td width="18%" class="picture">
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<div class="fileinput-new thumbnail">
								<img src="<?php echo base_url()."static/img/member-pic/".hash('md5', $row->name).$row->ext;?>" style="max-height:200px; width:100%;">
							</div>
							<div class="fileinput-preview fileinput-exists thumbnail"></div>
							<div>
								<span class="btn btn-default btn-xs btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
								<input type="file" name="pic1">
								</span>
								<a href="#" class="btn btn-default btn-xs fileinput-exists" data-dismiss="fileinput">Remove</a>
							</div>
						</div>


					</td>
					<td width="18%" class="picture2">
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<div class="fileinput-new thumbnail">
								<img src="<?php echo base_url()."static/img/member-life/".hash('md5', $row->name).$row->ext;?>" style="max-height:200px; width:100%;">
							</div>
							<div class="fileinput-preview fileinput-exists thumbnail"></div>
							<div>
								<span class="btn btn-default btn-xs btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
								<input type="file" name="pic2">
								</span>
								<a href="#" class="btn btn-default btn-xs fileinput-exists" data-dismiss="fileinput">Remove</a>
							</div>
						</div>
					</td>
					<td width="14%">
						<div>
							<span>姓名：</span>
							<span><?php echo $row->name;?></span>
						</div>

						<div>
							<span>年級：</span>
							<span>
								<select name="grade" class="form-control input-sm">
									<option value="1" <?php if($row->grade == 1){echo "selected";}?>>一</option>
									<option value="2" <?php if($row->grade == 2){echo "selected";}?>>二</option>
									<option value="3" <?php if($row->grade == 3){echo "selected";}?>>在職專班</option>
								</select>
							</span>
						</div>

					</td>
					<td width="50%" style="position: relative">
						<textarea name="intro"><?php $text = $row->introduction;$breaks = array("<br />","<br>","<br/>");$text = str_ireplace($breaks, "", $text);echo trim($text);?></textarea>
						<input type="hidden" name="sql_id" value="<?php echo $row->sql_id;?>">
						<div style="position:absolute; bottom:10px;right:10px;">
							<button type="submit" class="ｓａｖｅ btn btn-info btn-xs" data-id="<?php echo $row->sql_id;?>">儲存</button>
							<button type="button" class="delete btn btn-danger btn-xs" data-id="<?php echo $row->sql_id;?>">刪除</button>
						</div>
					</td>
				</tr>		
		</table>
		</form>
		<?php endforeach;?>
	</div>
	<div class="row padded old-member">
		<h4 class="content-title">畢業學長姊</h4>
		<div class="row padded">
			<div class="input-group add-member pull-right">
					<input type="text" id="member-name">
                    
					<button type="button" class="add btn btn-primary btn-xs">新增成員</button>
			</div>
		</div>
		<?php foreach($member_old->result() as $row):?>
			<div class="panel-group" id="accordion">
				<div class="panel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<div class="row">
								<div class="col-md-8 paper-name">
									<?php echo $row->paper_name;?>
								</div>
								<div class="col-md-2 paper-author">
									<?php echo $row->name;?>
								</div>
								<div class="col-md-2">
									<button type="button" class="modify btn btn-info btn-xs" data-id="<?php echo $row->sql_id;?>">修改</button>
									<button type="button" class="delete btn btn-danger btn-xs" data-id="<?php echo $row->sql_id;?>">刪除</button>
								</div>
							</div>
						</h4>
					</div>
					<div class="panel-body">
						<?php echo $row->paper_text;?>
					</div>
				</div>
			</div>
		<?php endforeach ?>
	</div>

	
</div>
</div>
</div>