<div class="container  main_page">
    <div class="row">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="http://placehold.it/1200x400" alt="...">
                </div>
                <div class="item">
                    <img src="http://placehold.it/1200x400." alt="...">
                </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <!-- <span class="glyphicon glyphicon-chevron-left"></span> -->
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <!-- <span class="glyphicon glyphicon-chevron-right"></span> -->
            </a>
        </div>
    </div>
    <div class="row homepage-info">
        <div class="col-sm-4 entry" id="intro">
            <h3>研究室簡介</h3>
            <h4><?php echo $intro; ?></h4>
        </div>
        <div class="col-sm-4 entry" id="news_">
            <h3>最新消息</h3>
            <div class="row" id="news">
                <?php foreach($news->result() as $row):?>
                <div class="col-xs-8">
                    <h4>
						<?php echo $row->notice; ?>
					</h4>
                </div>
                <div class="col-xs-4 time">
                    <h4>
						<?php echo $row->evt_time;?>
					</h4>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-sm-4 entry">
            <div class="row">
                <div class="col-xs-12">
                <h3>聯絡我們</h3>
                <h4>Email:yifeng@ie.nthu.edu.tw</h4>
                <h4>校內分機：42939</h4>
                <h4>校外：(03) 574-2939</h4>
                <h4>傳真：(03) 572-2685</h4>
                <h3>地址</h3>
                <h4>30043　新竹巿光復路二段101號</h4>
                <h4>清華大學工業工程與工程管理系</h4>
                <h4>辦公室：工程一館 918室 </h4>
                <h4>研究室：工程一館 907、704室</h4>
                </div>
            </div>
            
        </div>
    </div>
</div>
