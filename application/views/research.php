<div class="container  main_page research_page">
	<div class="row padded">
		<div class="col-md-3 hidden-sm hidden-xs">
			<div class="list-group padded" id="affix-list">
				<a href="#" class="	list-group-item disabled"><span class="title">研究領域</span></a>
				<?php foreach($project->result() as $row):?>
				<a href="#<?php echo md5($row->name);?>" class="list-group-item"><span class="subtitle"><?php echo $row->name;?></span></a>
				<?php endforeach;?>
			</div>
		</div>

		<div class="col-md-9">
			<?php foreach($project->result() as $row):?>
			<div class="panel panel-default" id="<?php echo md5($row->name);?>">
				<div class="panel-heading"><?php echo $row->name;?></div>
				<div class="panel-body">
					<?php echo $row->introduction;?>
				</div>

				<div class="panel-heading" data-toggle="dropdown" class="dropdown-toggle">相關計劃</div>
				<table class="table" >
					<thead>
						<tr>
							<th>計畫名稱</th>
							<th>合作單位</th>
							<th>計畫期間</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($plan->result() as $p):?>
						<?php if(!strcmp($p->source, $row->sql_id)):?>
						<tr>
							<td><?php echo $p->name;?></td>
							<td><?php echo $p->institution;?></td>
							<td><?php echo $p->start_date."~".$p->end_date;?></td>
						</tr>
						<?php endif;endforeach;?>
					</tbody>
				</table>
				
				<div class="panel-heading">相關論文</div>
				<table class="table table-striped">
					<tbody>
						<?php foreach($paper->result() as $r):?>
						<?php if(!strcmp($r->paper_cat, $row->name)):?>
						<tr>
						<td><?php echo $r->paper_name;?></td>
						</tr>
						<?php endif;endforeach;?>
						
					</tbody>
				</table>

			</div>
			<?php endforeach;?>

		</div>
	</div>
</div>
