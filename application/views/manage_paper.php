<div class="col-sm-9">
	<div class="row padded professor-paper">
		<h4 class="content-title">專長學科</h4>
		<table class="table table-bordered manage-table">
			<thead>
				<tr>
					<th>論文</th>
					<th width="8%"></th>
					<th width="8%">
						<button type="button" class="add btn btn-primary btn-xs">新增</button>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($paper->result() as $row):?>
				<tr>
					<td class="paper">
						<div class='inputdata paper_data' contenteditable='false'>
						<?php echo $row->paper;?>
						</div>		
					</td>
					<td class="handle">
						<button type="button" class="modify btn btn-info btn-xs" data-id="<?php echo $row->sql_id;?>">修改</button>
					</td>
					<td class="handle2">
						<button type="button" class="delete btn btn-danger btn-xs" data-id="<?php echo $row->sql_id;?>">刪除</button>
					</td>
				</tr>
				<?php endforeach;?>
			</tbody>
			<tfoot>
			</tfoot>
		</table>
	</div>
</div>
</div>
</div>