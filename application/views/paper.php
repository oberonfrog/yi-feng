<div class="container  main_page paper_page">
	<div class="row  padded">
		<div class="col-sm-3 hidden-xs">
			<div class="list-group padded" id="affix-list">
				<a class="list-group-item disabled"><span class="title">論文</span></a>
				<a class="list-group-item" href="#one"><span class="subtitle">期刊論文</span></a>
			</div>
		</div>
		<div class="col-sm-9">
			
			<div class="row" id="one">
				<table class="table table-bordered table-striped">
					<thead>
						<th>期刊論文</th>
					</thead>
					<tbody>
						<?php foreach($paper->result() as $row):?>
							<tr>
								<td><?php echo $row->paper;?></td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		
		</div>
	</div>
</div>
