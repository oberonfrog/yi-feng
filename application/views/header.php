<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Yi-FENG LAB</title>

	<!-- Bootstrap -->
	<link href="<?php echo base_url();?>static/bootstrap/css/magic-bootstrap.css" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	<link href="<?php echo base_url();?>static/css/module.css" rel="stylesheet">
	<?php if(isset($css) && !is_null($css)):?>
	<?php foreach($css as $row):?>
		<link href="<?php echo base_url();?>static/css/<?php echo $row;?>.css" rel="stylesheet">
	<?php endforeach;endif;?>

	
</head>
