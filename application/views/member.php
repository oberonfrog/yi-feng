<div class="container  main_page member_page">
	<div class="row padded">
		<div class="col-sm-3 hidden-xs" >
			<div class="list-group padded" id="affix-list">
				<a class="list-group-item disabled"><span class="title">在學學生</span></a>
				<a class="list-group-item" href="#one"><span class="subtitle">碩士班一年級</span></a>
				<a class="list-group-item" href="#two"><span class="subtitle">碩士班二年級</span></a>
				<a class="list-group-item" href="#work"><span class="subtitle">在職專班</span></a>
				<a class="list-group-item disabled"><span class="title">其他</span></a>
				<a class="list-group-item" href="#graduated"><span class="subtitle">歷屆學長姊</span></a>
			</div>
		</div>
		
		<div class="col-sm-9">
			<div class="row member" id="one">
				<h4 class="content-title">碩士班一年級</h4>
				<?php $one_count = 1; foreach($member->result() as $row):?>
					<?php if($row->grade == 1):?>
						<div class="first-year col-sm-4 col-xs-6" data-order="<?php echo $one_count++; ?>">
							<span><?php echo $row->name;?></span><br>
							<img src="<?php echo base_url()."static/img/member-pic/".hash('md5', $row->name).$row->ext;?>" width="150px">
						</div>
					<?php endif;?>
				<?php endforeach;?>
			</div>
			
			<div class="row member" id="two">
				<h4 class="content-title">碩士班二年級</h4>
				<?php $two_count = 1; foreach($member->result() as $row):?>
					<?php if($row->grade == 2):?>
						<div class="second-year col-sm-4 col-xs-6 padded" data-order="<?php echo $two_count++; ?>">
							<span><?php echo $row->name;?></span><br>
							<img src="<?php echo base_url()."static/img/member-pic/".hash('md5', $row->name).$row->ext;?>" width="150px">
						</div>
					<?php endif;?>
				<?php endforeach;?>
			</div>
			<div class="row member" id="work">
				<h4 class="content-title">碩士班在職專班</h4>
				<?php $work_count = 1; foreach($member->result() as $row):?>
					<?php if($row->grade == 3):?>
						<div class="work col-sm-4 col-xs-6" data-order="<?php echo $work_count++; ?>">
							<span><?php echo $row->name;?></span><br>
							<img src="<?php echo base_url()."static/img/member-pic/".hash('md5', $row->name).$row->ext;?>" width="150px">
						</div>
					<?php endif;?>
				<?php endforeach;?>
			</div>
			<div class="row member" id="graduated">
				<h4 class="content-title">歷屆學長姊</h4>
				
				<div class="panel-group" id="accordion">
					<?php $count=1; $now_year = 0; foreach($member_old->result() as $row):?>
                    <?php if($row->year != $now_year){$now_year = $row->year; echo $row->year;}?>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $count;?>">
									<div class="row">
										<div class="col-md-9">
											<?php echo $row->paper_name;?>

										</div>
										<div class="col-md-3 pull-right">
											<?php echo $row->name;?>
										</div>
									</div>
								</a>
					  		</h4>
						</div>
						<div id="collapse<?php echo $count++;?>" class="panel-collapse collapse">
							<div class="panel-body">
								<?php echo $row->paper_text;?>
							</div>
						</div>
					</div>
					<?php endforeach ?>
				</div>
				
			</div>
		</div>
	</div>
</div>
