<body>
    <header>
        <div class="container header">
            <div class="row padded">
                <div class="col-sm-4">
                    <a href="<?php echo site_url();?>">
						<img src="<?php echo base_url();?>static/img/logo.jpg" width="500">
					</a>
                </div>
                <div class="col-sm-6 col-sm-offset-2" style="margin-top:120px;">
                    <nav class="navbar container navbar-default" role="navigation" style="padding-left:0px;">
                        <div class="container">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>

                                </button>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <li class="<?php if(!strcmp($nav," advisor ")){echo 'active';} ?>">
                                        <a href="<?php echo site_url('index.php/advisor');?>">指導教授</a>
                                    </li>
                                    <li <?php if(!strcmp($nav, "member")){echo 'class="active"';} ?>>
                                        <a href="<?php echo site_url('index.php/member');?>">研究室成員</a>
                                    </li>
                                    <li <?php if(!strcmp($nav, "research")){echo 'class="active"';}?>>
                                        <a href="<?php echo site_url('index.php/research');?>">研究重點</a>
                                    </li>
                                    <li <?php if(!strcmp($nav, "paper")){echo 'class="active"';}?>>
                                        <a href="<?php echo site_url('index.php/paper');?>">論文</a>
                                    </li>
                                    <li <?php if(!strcmp($nav, "picture")){echo 'class="active"';}?>>
                                        <a href="<?php echo site_url('index.php/picture');?>">生活點滴</a>
                                    </li>
                                    <?php if(strcmp($user, "")):?>
                                    <li <?php if(!strcmp($nav, "manage")){echo 'class="active"';}?>>
                                        <a href="<?php echo site_url('index.php/manage/entry');?>">管理</a>
                                    </li>
                                    <?php endif ?>
                                    <?php if(strcmp($user, "")):?>
                                    <li>
                                        <a href="#" id="logout">登出</a>
                                    </li>
                                    <?php else:?>
                                    <li class="dropdown">
                                        <a class="dropdown-toggle" href="#" data-toggle="dropdown">登入<strong class="caret"></strong></a>
                                        <div class="dropdown-menu padded login" id="login-block">
                                            <form id="login-form">
                                                <div class="form-group">
                                                    <label for="id">帳號</label>
                                                    <input type="text" class="form-control" name="id" placeholder="帳號">
                                                </div>
                                                <div class="form-group">
                                                    <label for="password">密碼</label>
                                                    <input type="password" class="form-control" name="password" placeholder="密碼">
                                                </div>
                                                <div class="form-group">
                                                    <input type="hidden" class="form-control" name="url" value="<?php echo $nav;?>">
                                                </div>
                                                <button id="login" class="btn btn-default">登入</button>
                                            </form>
                                        </div>
                                    </li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>

                    </nav>
                </div>

            </div>
        </div>
    </header>