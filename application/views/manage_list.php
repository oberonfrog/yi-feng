<div class="container main_page">
	<div class="row padded">
		<div class="col-sm-3 hidden-xs" >
			<div class="list-group padded" id="affix-list">
				<a class="list-group-item" href="<?php echo site_url('index.php/manage/entry');?>"><span class="subtitle">首頁</span></a>
				<a class="list-group-item" href="<?php echo site_url('index.php/manage/advisor');?>"><span class="subtitle">指導教授</span></a>
				<a class="list-group-item" href="<?php echo site_url('index.php/manage/member');?>"><span class="subtitle">研究室成員</span></a>
				<a class="list-group-item" href="<?php echo site_url('index.php/manage/research');?>"><span class="subtitle">研究重點</span></a>
				<a class="list-group-item" href="<?php echo site_url('index.php/manage/paper');?>"><span class="subtitle">論文</span></a>
				<a class="list-group-item" href="<?php echo site_url('index.php/manage/picture');?>"><span class="subtitle">生活點滴</span></a>
			</div>
		</div>
		