<div class="col-sm-9">
	<div class="row padded professor-project">
		
		<h4 class="content-title">研究計劃</h4>
		<div class="row padded">
			<div class="input-group add-research pull-right">
					<input type="text" id="research-name" width="40%">
					<button type="button" class="add btn btn-primary btn-xs">新增計劃</button>
			</div>
		</div>
		
		<?php foreach($project->result() as $row):?>
		<div class="panel">
			<div class="panel-heading top">
				<span class="name"><?php echo $row->name;?></span>
				<div class="input-group pull-right">
					<button type="button" class="modify btn btn-info btn-xs" style="margin-right:15px;" data-id="<?php echo $row->sql_id;?>">修改</button>
					<button type="button" class="delete btn btn-danger btn-xs" style="margin-left:15px;" data-id="<?php echo $row->sql_id;?>">刪除</button>
				</div>
			</div>
			<div class="panel-body">
				<?php echo $row->introduction;?>
			</div>

			<div class="panel-heading plan" data-toggle="dropdown" class="dropdown-toggle">相關計劃<button type="button" class="add btn btn-primary btn-xs pull-right">新增</button></div>
			<table class="table plan">
				<thead>
					<tr>
						<th width="40%">計畫名稱</th>
						<th>合作單位</th>
						<th>開始期間</th>
						<th>結束時間</th>
						<th width="8%"></th>
						<th width="8%">
							
						</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($plan->result() as $p):?>
					<?php if(!strcmp($p->source, $row->sql_id)):?>
					<tr>
						<td class="name"><?php echo $p->name;?></td>
						<td class="institution"><?php echo $p->institution;?></td>
						<td class="start_date"><?php echo $p->start_date;?></td>
						<td class="end_date"><?php echo $p->end_date;?></td>
						<td><button type="button" class="modify btn btn-info btn-xs" data-id="<?php echo $p->sql_id;?>">修改</button></td>
						<td><button type="button" class="delete btn btn-danger btn-xs" data-id="<?php echo $p->sql_id;?>">刪除</button></td>
					</tr>
					<?php endif;endforeach;?>
				</tbody>
			</table>

			<div class="panel-heading paper">相關論文</div>
			<table class="table paper">
				<thead>
					<tr>
						<th>論文名稱</th>
						<th width="8%"></th>
						<th width="8%"></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($paper->result() as $r):?>
					<?php if(!strcmp($r->paper_cat, $row->name)):?>
					<tr>
					<td class="name"><?php echo $r->paper_name;?></td>
					<td><!--<button type="button" class="modify btn btn-info btn-xs" data-id="<?php echo $r->sql_id;?>">修改</button>--></td>
					<td><!--<button type="button" class="delete btn btn-danger btn-xs" data-id="<?php echo $r->sql_id;?>">刪除</button>--></td>
					</tr>
					<?php endif;endforeach;?>

				</tbody>
			</table>

		</div>
		<?php endforeach;?>
	</div>
</div>
</div>
</div>