	<footer>
		<div class="container">
			<div class="row">
				<div class="col-sm-4"></div>
                <div class="col-sm-4"></div>
				<div class="col-sm-4">
					<small>Copyright <?php echo date("Y");?> © SCM & APS Lab. All rights reserved. </small>
				</div>
			</div>
		</div>

	</footer>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="<?php echo base_url();?>static/js/jquery-1.11.1.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<?php echo base_url();?>static/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>static/js/login.js"></script>

	<?php if(isset($js) && !is_null($js)):?>
	<?php foreach($js as $row):?>
		<script src="<?php echo base_url();?>static/js/<?php echo $row;?>.js"></script>
	<?php endforeach;endif;?>

</body>

</html>