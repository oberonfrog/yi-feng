<div class="col-sm-9">
	<div class="row padded picture">
		<div class="row upload">
			<h4>新增照片</h4>
			<form action="<?php echo site_url('index.php/edit/picture/upload');?>" method="post" enctype="multipart/form-data" >
				<div class="fileinput input-group">
					<span class="input-group-addon"><span>標題</span></span>
					<input type="text" class="form-control" placeholder="title" name="title">
				</div>
				
				<div class="fileinput fileinput-new input-group" data-provides="fileinput">
					<span class="input-group-addon">
						<span>原圖</span>
					</span>
					<div class="form-control" data-trigger="fileinput">
						<span class="fileinput-filename"></span>
					</div>
					<span class="input-group-addon btn btn-default btn-file">
						<span class="fileinput-new">Select file</span>
					<span class="fileinput-exists">Change</span>
					<input type="file" name="origin" />
					</span>
					<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
				</div>
				<div class="fileinput fileinput-new input-group" data-provides="fileinput">
					<span class="input-group-addon">
						<span>縮圖</span>
					</span>
					<div class="form-control" data-trigger="fileinput">
						<span class="fileinput-filename"></span>
					</div>
					<span class="input-group-addon btn btn-default btn-file">
						<span class="fileinput-new">Select file</span>
					<span class="fileinput-exists">Change</span>
					<input type="file" name="short" />
					</span>
					<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
				</div>
				<button class="btn btn-primary pull-right" type="submit">上傳</button>
			</form>
		</div>
		<div class="row modify-picture">
			<h4>修改照片</h4>
			<?php foreach($picture->result() as $row):?>
			<div class="col-xs-3">
				<div class="thumbnail">
					<img src="<?php echo base_url()."static/img/picture/thumbnail/".$row->name;?>" class="img-thumbnail" alt="Sample Image">
					<div class="caption">
						<div class="picture-title"><?php echo $row->title;?></div>
						<div class="input-group pull-right picture-modify-button">
							<button class="delete btn btn-danger btn-xs" data-id="<?php echo $row->sql_id;?>">刪除</button>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach;?>
		</div>
	</div>



</div>
</div>
</div>