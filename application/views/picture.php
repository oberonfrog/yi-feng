<div class="container  main_page">
	<div class="row padded">
		<div class="col-sm-12">
			<div id="blueimp-gallery" class="blueimp-gallery  blueimp-gallery-controls" >
				<div class="slides"></div>
				<h3 class="title"></h3>
				<a class="prev">‹</a>
				<a class="next">›</a>
				<a class="close">×</a>
				<a class="play-pause"></a>
				<ol class="indicator"></ol>
			</div>

			<div id="links">
				<?php foreach($path->result() as $row):?>
				<a href="<?php echo base_url()."static/img/picture/".$row->name;?>" title="<?php echo $row->title;?>">
					<img class="picture-thumb" src="<?php echo base_url()."static/img/picture/thumbnail/".$row->name;?>" alt="<?php echo $row->title;?>">
				</a>
				<?php endforeach;?>
				
			</div>





		</div>
	</div>
</div>