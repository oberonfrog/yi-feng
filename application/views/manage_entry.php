<div class="col-sm-9">
	<div class="row padded introduction">
		<h4 class="content-title">研究室簡介</h4>

		<div id="input" contenteditable="false">
			<?php echo $intro->result()[0]->introduction;?></div><br>
		<button type="button" class="btn btn-primary pull-right content-modify">修改</button>
	</div>
	<div class="row padded latest-news">
		<h4 class="content-title">最新消息</h4>
		<table class="table table-bordered manage-table">
			<thead>
				<tr>
					<th width="70%">內容</th>
					<th width="14%">時間</th>
					<th width="8%"></th>
					<th width="8%">
						<button type="button" class="add btn btn-primary btn-xs">新增</button>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($news->result() as $row):?>
				<tr>
					<td class="notice">
						<?php echo $row->notice;?></td>
					<td class="time">
						<?php echo $row->evt_time;?></td>
					<td class="handle">
						<button type="button" class="modify btn btn-info btn-xs" data-id="<?php echo $row->sql_id;?>">修改</button>
					</td>
					<td class="handle2">
						<button type="button" class="delete btn btn-danger btn-xs" data-id="<?php echo $row->sql_id;?>">刪除</button>
					</td>
				</tr>
				<?php endforeach;?>
			</tbody>
			<tfoot>
			</tfoot>
		</table>
	</div>

</div>
</div>
</div>