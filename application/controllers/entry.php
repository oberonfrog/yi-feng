<?php

	class Entry extends CI_Controller{
		
		function index(){
			$this->load->helper('url');
			$this->load->model('Entry_model');
			$user = $this->session->userdata('user');
			$nav_data = array(
				'nav'    =>    "entry",
				'user'	 =>	   $user
			);
			$news = $this->Entry_model->news();
			$intro = $this->Entry_model->intro();
			$entry_data = array(
				'news'	=> $news,
				'intro' => $intro->result()[0]->introduction
			);

			$this->load->view('header');
			$this->load->view('navbar', $nav_data);
			$this->load->view('entry', $entry_data);
			$this->load->view('footer');
		}
	}

?> 