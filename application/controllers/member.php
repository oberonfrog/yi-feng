<?php

	class Member extends CI_Controller{
		
		function index()
		{
			$user = $this->session->userdata('user');
			$this->load->model('Member_model');
			$member_name = $this->Member_model->member_name();
			$member_history = $this->Member_model->member_history();
			$nav_data = array(
				'nav'    =>    "member",
				'user'	 =>	   $user
			);
			$member_data = array(
				'member' 	 =>	   $member_name,
				'member_old' =>    $member_history 
			);
			$include_js = array(
				'js'	=>    array(
					'member-animation',
					'list-affix',
					'md5'
				)
			);
			$this->load->helper('url');
			$this->load->view('header');
			$this->load->view('navbar', $nav_data);
			$this->load->view('member', $member_data);
			$this->load->view('footer', $include_js);
		}
		
		function fetch($name)
		{
			$this->load->helper('url');
			$this->load->model('Member_model');
			$result = $this->Member_model->member_intro($name)->result()[0];
			$intro = $result->introduction;
			$ext = $result->ext;
			$arr = array(
				'intro'   => $intro,
				'ext'     => $ext
			);    
			header('Content-Type: application/json');
			echo json_encode($arr);
		}
	}

?>