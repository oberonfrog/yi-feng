<?php

	class Manage extends CI_Controller{
		
		function index()
		{
			$this->load->helper('url');
			redirect('index.php','refresh');
		}
		
		function entry()
		{
			$this->load->helper('url');
			$this->load->model('Manage_model');
			$login = $this->session->userdata('login');
			
			if(!$login)
			{
				redirect('index.php','refresh');
			}
			
			
			$user = $this->session->userdata('user');
			$news = $this->Manage_model->get_news();
			$intro = $this->Manage_model->get_intro();
			
			$nav_data = array(
				'nav'    =>    'manage',
				'user'	 =>	   $user
			);
			$entry_data = array(
				'intro'  =>    $intro,
				'news'   =>    $news
			);
			$include_css = array(
				'css'	=>	  array(
					'datepicker',
					'manage'
				)
			);
			$include_js = array(
				'js'	=>    array(
					'bootstrap-datepicker',
					'list-affix',
					'pasteHTML',
					'manage-entry'
				)
			);
			$this->load->helper('url');
			$this->load->view('header', $include_css);
			$this->load->view('navbar', $nav_data);
			$this->load->view('manage_list');
			$this->load->view('manage_entry', $entry_data);
			$this->load->view('footer', $include_js);
		}
		function advisor()
		{
			$this->load->helper('url');
			$this->load->model('Manage_model');
			$login = $this->session->userdata('login');
			
			if(!$login)
			{
				redirect('index.php','refresh');
			}
			$user = $this->session->userdata('user');
			$exp = $this->Manage_model->get_exp();
			$speciality = $this->Manage_model->get_speciality();
			$nav_data = array(
				'nav'    =>    'manage',
				'user'	 =>	   $user
			);
			
			$advisor_data = array(
				'exp'	     =>    $exp,
				'speciality' =>    $speciality
			);
			
			$include_css = array(
				'css'	=>	  array(
					'datepicker',
					'manage'
				)
			);
			$include_js = array(
				'js'	=>    array(
					'bootstrap-datepicker',
					'list-affix',
					'manage-advisor'
				)
			);
			$this->load->helper('url');
			$this->load->view('header', $include_css);
			$this->load->view('navbar', $nav_data);
			$this->load->view('manage_list');
			$this->load->view('manage_advisor', $advisor_data);
			$this->load->view('footer', $include_js);
			
		}
		function member()
		{
			$this->load->helper('url');
			$this->load->model('Manage_model');
			$login = $this->session->userdata('login');
			
			if(!$login)
			{
				redirect('index.php','refresh');
			}
			$user = $this->session->userdata('user');
			$member = $this->Manage_model->get_member();
			$member_old = $this->Manage_model->get_old_member();
			$nav_data = array(
				'nav'    =>    'manage',
				'user'	 =>	   $user
			);
			
			$member_data = array(
				'member'	     =>    $member,
				'member_old'     =>    $member_old
			);
			
			$include_css = array(
				'css'	=>	  array(
					'manage',
					'jasny-bootstrap.min'
				)
			);
			$include_js = array(
				'js'	=>    array(
					'list-affix',
					'pasteHTML',
					'manage-member',
					'jasny-bootstrap.min'
				)
			);
			$this->load->helper('url');
			$this->load->view('header', $include_css);
			$this->load->view('navbar', $nav_data);
			$this->load->view('manage_list');
			$this->load->view('manage_member', $member_data);
			$this->load->view('footer', $include_js);
			
		}
		function paper()
		{
			$this->load->helper('url');
			$this->load->model('Manage_model');
			$login = $this->session->userdata('login');
			
			if(!$login)
			{
				redirect('index.php','refresh');
			}
			$user = $this->session->userdata('user');
			$paper = $this->Manage_model->get_professor_paper();
			$speciality = $this->Manage_model->get_speciality();
			$nav_data = array(
				'nav'    =>    'manage',
				'user'	 =>	   $user
			);
			
			$paper_data = array(
				'paper'	     =>    $paper
			);
			
			$include_css = array(
				'css'	=>	  array(
					'manage'
				)
			);
			$include_js = array(
				'js'	=>    array(
					'list-affix',
					'manage-paper'
				)
			);
			$this->load->helper('url');
			$this->load->view('header', $include_css);
			$this->load->view('navbar', $nav_data);
			$this->load->view('manage_list');
			$this->load->view('manage_paper', $paper_data);
			$this->load->view('footer', $include_js);
			
		}
		
		function research()
		{
			$this->load->helper('url');
			$this->load->model('Manage_model');
			$login = $this->session->userdata('login');
			
			if(!$login)
			{
				redirect('index.php','refresh');
			}
			$user = $this->session->userdata('user');
			$project = $this->Manage_model->get_project();
			$plan = $this->Manage_model->get_plan();
			$paper = $this->Manage_model->get_paper();
			$nav_data = array(
				'nav'    =>    'manage',
				'user'	 =>	   $user
			);
			
			$research_data = array(
				'project' =>	$project,
				'plan'    =>	$plan,
				'paper'   =>	$paper
			);
			
			$include_css = array(
				'css'	=>	  array(
					'datepicker',
					'manage'
				)
			);
			$include_js = array(
				'js'	=>    array(
					'bootstrap-datepicker',
					'list-affix',
					'manage-research'
				)
			);
			$this->load->helper('url');
			$this->load->view('header', $include_css);
			$this->load->view('navbar', $nav_data);
			$this->load->view('manage_list');
			$this->load->view('manage_research', $research_data);
			$this->load->view('footer', $include_js);
			
		}
		
		function picture()
		{
			$this->load->helper('url');
			$this->load->model('Manage_model');
			$login = $this->session->userdata('login');
			
			if(!$login)
			{
				redirect('index.php','refresh');
			}
			$user = $this->session->userdata('user');
			$picture = $this->Manage_model->get_picture();
			$nav_data = array(
				'nav'    =>    'manage',
				'user'	 =>	   $user
			);
			
			$picture_data = array(
				'picture'	 =>	   $picture
			);
			$include_css = array(
				'css'	=>	  array(
					'manage',
					'jasny-bootstrap.min'
				)
			);
			$include_js = array(
				'js'	=>    array(
					'list-affix',
					'manage-picture',
					'jasny-bootstrap.min',
					'holder',
					'pasteHTML'
				)
			);
			$this->load->helper('url');
			$this->load->view('header', $include_css);
			$this->load->view('navbar', $nav_data);
			$this->load->view('manage_list');
			$this->load->view('manage_picture', $picture_data);
			$this->load->view('footer', $include_js);
			
		}
		
	}

?>