<?php

	class Picture extends CI_Controller{
		
		function index(){
			$this->load->helper('url');
			$this->load->model('Picture_model');
			
			$user = $this->session->userdata('user');
			$picture_path = $this->Picture_model->load();
			$nav_data = array(
				'nav'    =>    "picture",
				'user'	 =>	   $user
			);
			$picture_data = array(
				'path'	 =>	   $picture_path
			);
			$include_js = array(
				'js'	=>    array(
					'blueimp-gallery',
					'gallery-controller',
					'holder'
				)
			);
			$include_css = array(
				'css'	=>	  array(
					'blueimp-gallery.min'
				)
			);
			$this->load->view('header',$include_css);
			$this->load->view('navbar', $nav_data);
			$this->load->view('picture', $picture_data);
			$this->load->view('footer', $include_js);
		}
	}

?>