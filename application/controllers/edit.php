<?php

	class Edit extends CI_Controller{
		
		function news($event){
			$this->load->helper('url');
			$this->load->model('Edit_model');
			$login = $this->session->userdata('login');
			
			if(!$login)
			{
				redirect('index.php','refresh');
			}
			if(!strcmp($event, "add"))
			{
				echo $this->Edit_model->news_add();
			}
			else if(!strcmp($event, "modify"))
			{
				$this->Edit_model->news_modify();
			}
			else if(!strcmp($event, "delete"))
			{
				$this->Edit_model->news_delete();
			}
		}
		
		function introduction()
		{
			$this->load->helper('url');
			$this->load->model('Edit_model');
			$login = $this->session->userdata('login');
			
			if(!$login)
			{
				redirect('index.php','refresh');
			}
			$this->Edit_model->intro_modify();
		}
		
		function exp($event)
		{
			$this->load->helper('url');
			$this->load->model('Edit_model');
			$login = $this->session->userdata('login');
			if(!$login)
			{
				redirect('index.php','refresh');
			}
			if(!strcmp($event, "add"))
			{
				echo $this->Edit_model->exp_add();
			}
			else if(!strcmp($event, "modify"))
			{
				echo $this->Edit_model->exp_modify();
			}
			else if(!strcmp($event, "delete"))
			{
				$this->Edit_model->exp_delete();
			}

		}
		function speciality($event)
		{
			$this->load->helper('url');
			$this->load->model('Edit_model');
			$login = $this->session->userdata('login');
			if(!$login)
			{
				redirect('index.php','refresh');
			}
			if(!strcmp($event, "add"))
			{
				echo $this->Edit_model->speciality_add();
			}
			else if(!strcmp($event, "modify"))
			{
				echo $this->Edit_model->speciality_modify();
			}
			else if(!strcmp($event, "delete"))
			{
				$this->Edit_model->speciality_delete();
			}
		}
		
		function paper($event)
		{
			$this->load->helper('url');
			$this->load->model('Edit_model');
			$login = $this->session->userdata('login');
			if(!$login)
			{
				redirect('index.php','refresh');
			}
			if(!strcmp($event, "add"))
			{
				echo $this->Edit_model->paper_add();
			}
			else if(!strcmp($event, "modify"))
			{
				echo $this->Edit_model->paper_modify();
			}
			else if(!strcmp($event, "delete"))
			{
				$this->Edit_model->paper_delete();
			}
		}
		
		function research($event, $handler)
		{
			$this->load->helper('url');
			$this->load->model('Edit_model');
			$login = $this->session->userdata('login');
			if(!$login)
			{
				redirect('index.php','refresh');
			}
			if(!strcmp($event, "add"))
			{
				if(!strcmp($handler, "main"))
				{
					echo $this->Edit_model->research_add();
				}
				else if(!strcmp($handler, "plan"))
				{
					echo $this->Edit_model->relative_plan_add();
				}
				if(!strcmp($handler, "paper"))
				{
					echo $this->Edit_model->relative_paper_add();
				}
			}
			else if(!strcmp($event, "modify"))
			{
				if(!strcmp($handler, "main"))
				{
					echo $this->Edit_model->research_modify();
				}
				else if(!strcmp($handler, "plan"))
				{
					echo $this->Edit_model->relative_plan_modify();
				}
				else if(!strcmp($handler, "paper"))
				{
					echo $this->Edit_model->relative_paper_modify();
				}
			}
			else if(!strcmp($event, "delete"))
			{
				if(!strcmp($handler, "main"))
				{
					echo $this->Edit_model->research_delete();
				}
				else if(!strcmp($handler, "plan"))
				{
					echo $this->Edit_model->relative_plan_delete();
				}
				else if(!strcmp($handler, "paper"))
				{
					echo $this->Edit_model->relative_paper_delete();
				}
			}
		}
		function picture($event)
		{
			$this->load->helper('url');
			$this->load->helper('file');
			$this->load->model('Edit_model');
			$login = $this->session->userdata('login');
			if(!$login)
			{
				redirect('index.php','refresh');
			}
			if(!strcmp($event, "upload"))
			{
				$this->Edit_model->picture_upload();
                redirect('index.php/manage/picture','refresh');
			}
			else if(!strcmp($event, "delete"))
			{
				$name = trim($this->Edit_model->picture_delete());
				unlink("static/img/picture/" . $name);
				unlink("static/img/picture/thumbnail/" . $name);
			}
		}
		function member($event)
		{
			
			$this->load->helper('url');
			$this->load->helper('file');
			$this->load->model('Edit_model');
			$login = $this->session->userdata('login');
			if(!$login)
			{
				redirect('index.php','refresh');
			}
			if(!strcmp($event, "modify"))
			{
				echo $this->Edit_model->member_modify();
				redirect('index.php/manage/member','refresh');
			}
			else if(!strcmp($event, "add"))
			{
				$this->Edit_model->member_add();
			}
			else if(!strcmp($event, "delete"))
			{
				$name = $this->Edit_model->member_delete();
				unlink("static/img/member-pic/" . $name);
				unlink("static/img/member-life/" . $name);
			}
		}
		function old_member($event)
		{
			
			$this->load->helper('url');
			$this->load->helper('file');
			$this->load->model('Edit_model');
			$login = $this->session->userdata('login');
			if(!$login)
			{
				redirect('index.php','refresh');
			}
			if(!strcmp($event, "modify"))
			{
				echo $this->Edit_model->old_member_modify();
			}
			else if(!strcmp($event, "add"))
			{
				echo json_encode($this->Edit_model->old_member_add());
			}
			else if(!strcmp($event, "delete"))
			{
				echo $this->Edit_model->old_member_delete();
			}
		}
	}

?> 