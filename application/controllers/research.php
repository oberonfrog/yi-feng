<?php

	class Research extends CI_Controller{
		
		function index(){
			$user = $this->session->userdata('user');
			$this->load->model('Research_model');
			$project = $this->Research_model->project();
			$plan = $this->Research_model->plan();
			$paper = $this->Research_model->paper();

			$nav_data = array(
				'nav'    =>    "research",
				'user'	 =>	   $user
			);
			$research_data = array(
				'project' =>	$project,
				'plan'    =>	$plan,
				'paper'   =>    $paper
			);
			$include_js = array(
				'js'	=>    array(
					'list-affix'
				)
			);
			$this->load->helper('url');
			$this->load->view('header');
			$this->load->view('navbar', $nav_data);
			$this->load->view('research', $research_data);
			$this->load->view('footer', $include_js);
		}
	}

?>