<?php

	class Paper extends CI_Controller{
		
		function index(){
			$this->load->helper('url');
			$this->load->model('Paper_model');
			
			$user = $this->session->userdata('user');
			$nav_data = array(
				'nav'    =>    "paper",
				'user'	 =>	   $user
			);
			
			$paper = $this->Paper_model->paper();
			$paper_data = array(
				'paper'			     =>	   $paper	
			);
			$include_js = array(
				'js'	=>    array(
					'list-affix',
					'paper-animation'
				)
			);
			$this->load->view('header');
			$this->load->view('navbar', $nav_data);
			$this->load->view('paper', $paper_data);
			$this->load->view('footer', $include_js);
		}
	}

?>