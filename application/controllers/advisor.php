<?php

	class Advisor extends CI_Controller{
		
		function index(){
			$this->load->model('Advisor_model');
			
			$user = $this->session->userdata('user');
			$nav_data = array(
				'nav'    =>    "advisor",
				'user'	 =>	   $user
			);
			$exp = $this->Advisor_model->exp();
			$speciality = $this->Advisor_model->speciality();
			$advisor_data = array(
				'exp'			=>	$exp,
				'speciality'	=>	$speciality
			);
			$this->load->helper('url');
			$this->load->view('header');
			$this->load->view('navbar', $nav_data);
			$this->load->view('advisor', $advisor_data);
			$this->load->view('footer');
		}
	}

?>