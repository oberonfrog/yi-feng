var hides = 7;
$('tbody tr').slice(7).hide();
$(window).scroll(function () {
    if ($(document).height()-200 <= $(window).scrollTop() + $(window).height()) {
        $('tbody tr').slice(hides, hides + 2).fadeIn('slow');
        hides += 2;
    }
});