/*jshint multistr: true */
/*global $:false */
/*jslint browser:true */
$(document).ready(function(){
	$("button.delete").prop("disabled",false);
	
	$(document).on('click', 'div.professor-project div.add-research button.add', function(){
		var main = $(this).parent();
		var that = $(this);
		var title = main.find('#research-name').val();
		var path = "http://" + window.location.host + "/index.php/edit/research/add/main";
		var ajaxdata = {
			'title': title
		};
		
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(id){
				that.parent().parent().after("<div class='panel'>\
						<div class='panel-heading top'>\
							<span class='name'>"+title+"</span>\
							<div class='input-group pull-right'>\
								<button type='button' class='modify btn btn-info btn-xs' style='margin-right:15px;' data-id='"+id+"'>修改</button>\
								<button type='button' class='delete btn btn-danger btn-xs' style='margin-left:15px;' data-id='"+id+"'>刪除</button>\
							</div>\
						</div>\
						<div class='panel-body'>\
						</div>\
						<div class='panel-heading plan' data-toggle='dropdown' class='dropdown-toggle'>相關計劃<button type='button' class='add btn btn-primary btn-xs pull-right'>新增</button></div>\
						<table class='table plan'>\
							<thead>\
								<tr>\
									<th width='40%'>計畫名稱</th>\
									<th>合作單位</th>\
									<th>開始期間</th>\
									<th>結束時間</th>\
									<th width='8%'></th>\
									<th width='8%'>\
									</th>\
								</tr>\
							</thead>\
							<tbody>\
							</tbody>\
						</table>\
						<div class='panel-heading paper'>相關論文<button type='button' class='add btn btn-primary btn-xs pull-right'>新增</button></div>\
						<table class='table paper'>\
							<thead>\
								<tr>\
									<th>論文名稱</th>\
									<th width='8%'></th>\
									<th width='8%'></th>\
								</tr>\
							</thead>\
							<tbody>\
							</tbody>\
						</table>\
					</div>");
				
			}
		});
		return false;
	});
	
	$(document).on('click', 'div.panel-heading button.modify', function(){
		var panel_heading = $(this).parent().parent();
		var title = panel_heading.find('.name').text();
		$(this).removeClass('modify').addClass('save').text("儲存");
		panel_heading.siblings('.panel-body').prop('contenteditable',true).addClass('editable');
		panel_heading.find('span.name').html("<input class='inputdata title_data' style='width:70%;' type='text' value=" + title + ">");
		return false;
	});
	$(document).on('click', 'div.panel-heading button.save', function(){

		var that = $(this);
		var panel_heading = $(this).parent().parent();
		var title = panel_heading.find('input.title_data').val();
		var intro = panel_heading.siblings('.panel-body').text();
		
		var path = "http://" + window.location.host + "/index.php/edit/research/modify/main";
		var ajaxdata = {
			'title': title,
			'intro': intro,
			'sql_id': $(this).attr('data-id')
		};
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(msg){
				panel_heading.siblings('.panel-body').prop('contenteditable',false).removeClass('editable');
				panel_heading.find('span.name').text(title);
				that.removeClass('save').addClass('modify').text("修改"); 
			}
		});
		return false;
	});
	$(document).on('click', 'div.panel-heading button.delete', function(){
		var path = "http://" + window.location.host + "/index.php/edit/research/delete/main";
		var that = $(this);
		var ajaxdata = {
			'sql_id': $(this).attr('data-id')
		};
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(){
				that.parent().parent().parent().hide(1000, function(){
					$(this).remove();
				});
			}
		});
		return false;
	});
	
	
	$(document).on('keydown', 'div.panel-body', function(e) {
		if (e.keyCode === 13) {
		  pasteHtmlAtCaret('<br>');
		  return false;
		}
	});


	
	$(document).on('click', 'div.professor-project table.plan button.modify', function(){
		var focus_tr = $(this).parent().parent();
		var name = $.trim(focus_tr.find('.name').text());
		var institution = $.trim(focus_tr.find('.institution').text());
		var start_date = $.trim(focus_tr.find('.start_date').text());
		var end_date = $.trim(focus_tr.find('.end_date').text());
		focus_tr.find('.name').html("<input class='inputdata name_data' type='text' value='"+name+"' />");
		focus_tr.find('.institution').html("<input class='inputdata institution_data' type='text' value='"+institution+"' />");
		focus_tr.find('.start_date').html("<input class='inputdata start_date_data datepicker' value='"+ start_date +"' data-date-format='yyyy-mm-dd'>");
		focus_tr.find('.end_date').html("<input class='inputdata end_date_data datepicker' value='"+ end_date +"' data-date-format='yyyy-mm-dd'>");
		$(this).removeClass('modify').addClass('save').text("儲存");
		focus_tr.find('.delete').prop('disabled',true);
		$('.datepicker').datepicker();
		
		return false;
	});
	$(document).on('click', 'div.professor-project table.plan button.save', function(){
		var that = $(this);
		var focus_tr = $(this).parent().parent();
		var name = $.trim(focus_tr.find('input.name_data').val());
		var institution = $.trim(focus_tr.find('input.institution_data').val());
		var start_date = $.trim(focus_tr.find('input.start_date_data').val());
		var end_date = $.trim(focus_tr.find('input.end_date_data').val());
		var path = "http://" + window.location.host + "/index.php/edit/research/modify/plan";
		var ajaxdata = {
			'name': name,
			'institution': institution,
			'start_date': start_date,
			'end_date': end_date,
			'sql_id': $(this).attr('data-id')
		};
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(msg){
				focus_tr.find('.name').text(name);
				focus_tr.find('.institution').text(institution);
				focus_tr.find('.start_date').text(start_date);
				focus_tr.find('.end_date').text(end_date);
				that.removeClass('save').addClass('modify').text("修改");
				focus_tr.find('.delete').prop('disabled',false);
			}
		});
		return false;
	});
	$(document).on('click', 'div.professor-project table.plan button.delete', function(){
		var that = $(this);
		var path = "http://" + window.location.host + "/index.php/edit/research/delete/plan";
		var ajaxdata = {
			'sql_id': $(this).attr('data-id')
		};
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(){
				that.parent().parent().hide(800, function(){
					$(this).remove();
				});
			}
		});
		return false;
	});
	
	
	$(document).on('click', 'div.professor-project div.plan button.add', function(){
		$('div.professor-project table.plan tbody').prepend('<tr>\
				<td class="name">\
					<input class="inputdata name_data" type="text"/></td>\
				<td class="institution">\
					<input class="inputdata institution_data" type="text"/></td>\
				<td class="start_date">\
					<input class="inputdata start_date_data datepicker" data-date-format="yyyy-mm-dd"></td>\
				<td class="end_date">\
					<input class="inputdata end_date_data datepicker" data-date-format="yyyy-mm-dd"></td>\
				<td class="handle">\
					<button type="button" class="additem btn btn-info btn-xs">加入</button>\
				</td>\
				<td class="handle2">\
					<button type="button" class="delitem btn btn-danger btn-xs">取消</button>\
				</td>\
			</tr>');
		$('.datepicker').datepicker();
		return false;
	});
	$(document).on('click', 'div.professor-project table.plan button.additem', function(){
		var that = $(this);
		var focus_tr = $(this).parent().parent();
		var name = $.trim(focus_tr.find('input.name_data').val());
		var institution = $.trim(focus_tr.find('input.institution_data').val());
		var start_date = $.trim(focus_tr.find('input.start_date_data').val());
		var end_date = $.trim(focus_tr.find('input.end_date_data').val());
		
		var path = "http://" + window.location.host + "/index.php/edit/research/add/plan";
		var ajaxdata = {
			'name': name,
			'institution': institution,
			'start_date': start_date,
			'end_date': end_date,
			'source': $(this).closest('div.panel').find('.top').find('button.modify').attr('data-id')
		};
		
		
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(id){
				focus_tr.find('.name').text(name);
				focus_tr.find('.institution').text(institution);
				focus_tr.find('.start_date').text(start_date);
				focus_tr.find('.end_date').text(end_date);
				that.removeClass('additem').attr("data-id", $.trim(id)).addClass('modify').text("修改");
				focus_tr.find('.delitem').removeClass('delitem').attr("data-id", $.trim(id)).addClass('delete').text("刪除");
			}
		});
			
		return false;
	});
	$(document).on('click', 'div.professor-project table.plan button.delitem', function(){
		$(this).parent().parent().remove();
		return false;
	});
	
	
	
	$(document).on('click', 'div.professor-project table.paper button.modify', function(){
		var focus_tr = $(this).parent().parent();
		var name = $.trim(focus_tr.find('.name').text());
		focus_tr.find('.name').html("<input class='inputdata name_data' type='text' value='"+name+"' />");
		$(this).removeClass('modify').addClass('save').text("儲存");
		focus_tr.find('.delete').prop('disabled',true);
		
		return false;
	});
	$(document).on('click', 'div.professor-project table.paper button.save', function(){
		var that = $(this);
		var focus_tr = $(this).parent().parent();
		var name = $.trim(focus_tr.find('input.name_data').val());
		var path = "http://" + window.location.host + "/index.php/edit/research/modify/paper";
		var ajaxdata = {
			'name': name,
			'sql_id': $(this).attr('data-id')
		};
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(){
				focus_tr.find('.name').text(name);
				that.removeClass('save').addClass('modify').text("修改");
				focus_tr.find('.delete').prop('disabled',false);
			}
		});
		return false;
	});
	$(document).on('click', 'div.professor-project table.paper button.delete', function(){
		var that = $(this);
		var path = "http://" + window.location.host + "/index.php/edit/research/delete/paper";
		var ajaxdata = {
			'sql_id': $(this).attr('data-id')
		};
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(){
				that.parent().parent().hide(800, function(){
					$(this).remove();
				});
			}
		});
		return false;
	});
	
	
	$(document).on('click', 'div.professor-project div.paper button.add', function(){
		$('div.professor-project table.paper tbody').prepend('<tr>\
				<td class="name">\
					<input class="inputdata name_data" type="text"/></td>\
				<td class="handle">\
					<button type="button" class="additem btn btn-info btn-xs">加入</button>\
				</td>\
				<td class="handle2">\
					<button type="button" class="delitem btn btn-danger btn-xs">取消</button>\
				</td>\
			</tr>');
		return false;
	});
	$(document).on('click', 'div.professor-project table.paper button.additem', function(){
		var that = $(this);
		var focus_tr = $(this).parent().parent();
		var name = $.trim(focus_tr.find('input.name_data').val());

		
		var path = "http://" + window.location.host + "/index.php/edit/research/add/paper";
		var ajaxdata = {
			'name': name,
			'source': $(this).closest('div.panel').find('.top').find('button.modify').attr('data-id')
		};
		
		
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(id){
				focus_tr.find('.name').text(name);
				that.removeClass('additem').attr("data-id", $.trim(id)).addClass('modify').text("修改");
				focus_tr.find('.delitem').removeClass('delitem').attr("data-id", $.trim(id)).addClass('delete').text("刪除");
			}
		});
			
		return false;
	});
	$(document).on('click', 'div.professor-project table.paper button.delitem', function(){
		$(this).parent().parent().remove();
		return false;
	});
});