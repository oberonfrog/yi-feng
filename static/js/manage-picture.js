$(document).ready(function(){
	$("button.delete").prop("disabled", false);
	$(document).on('click', 'div.modify-picture button.delete', function(){
		var path = "http://" + window.location.host + "/index.php/edit/picture/delete";
		var that = $(this);
		var ajaxdata = {
			'sql_id': $(this).attr('data-id')
		};
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(msg){
				console.log(msg);
				that.parent().parent().parent().parent().hide(1000, function(){
					$(this).remove();
				});
			}
		});
		return false;
	});
});