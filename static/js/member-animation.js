/*jshint multistr: true */
/*global $:false */
/*jslint browser:true */
$(document).ready(function(){
	var width = $(window).width();
	var num = width >= 768 ? 3 : 2;
	
	for(var i = 0, l = $('.first-year').length; i < l; i += num) {
		$('.first-year').slice(i, i+num).wrapAll('<div class="row"></div>');
	}
	for(i = 0, l = $('.second-year').length; i < l; i += num) {
		$('.second-year').slice(i, i+num).wrapAll('<div class="row"></div>');
	}
	for(i = 0, l = $('.work').length; i < l; i += num) {
		$('.work').slice(i, i+num).wrapAll('<div class="row"></div>');
	}
	
	
	$(".first-year, .second-year, .work").each(function(){
		$(this).children("img,span").on("click",function(){
			var order = parseInt($(this).parent("div").attr("data-order"));
			var data_key = MD5($(this).parent("div").find('span').text());
			var that = $(this);
			var path = "http://" + window.location.host + "/index.php/member/fetch/" + data_key;
			
			
			if(parseInt($(".intro-block").attr("data-from")) == order){
				$(".intro-row").slideUp(1000, function(){
					$(this).remove();
				});
				
			}
			else{
				if($(".intro-row").length > 0){
					$(".intro-row").slideUp(1000, function(){
						$(this).remove();
						$.ajax({
							type: 'POST',
							url: path,
							dataType: 'JSON',
							success: function(data){
								var picture_path = "http://" + window.location.host + "/static/img/member-life/" + data_key + data.ext;
								var data = '<div class="row intro-row padded" >\
										<div class="col-sm-12 intro-block"  data-from="' + order + '">\
											<div class="media" data-from="'+order+'">\
												<a class="pull-left" href="#">\
													<img class="media-object" src="'+ picture_path +'" style="max-height:200px; width:150px;">\
												</a>\
												<div class="media-body">\
													' + data.intro + '\
												</div>\
											</div>\
										</div>\
									</div>';
								that.parent().parent().after(data);
								$(".intro-row").slideDown(1000);
							}
						});
					});
				}
				else{
					$.ajax({
						type: 'POST',
						url: path,
						dataType: 'JSON',
						success: function(data){
							var picture_path = "http://" + window.location.host + "/static/img/member-life/" + data_key + data.ext;
							var data = '<div class="row intro-row padded" >\
									<div class="col-sm-12 intro-block"  data-from="' + order + '">\
										<div class="media" data-from="'+order+'">\
											<a class="pull-left" href="#">\
												<img class="media-object" src="'+ picture_path +'" style="max-height:200px; width:150px;">\
											</a>\
											<div class="media-body">\
												' + data.intro + '\
											</div>\
										</div>\
									</div>\
								</div>';
							that.parent().parent().after(data);
							$(".intro-row").slideDown(1000);
						}
					});
				}
				
			}
		});
	});
	
	
	
	
});

var hides = 2;
$('div#graduated div.panel').slice(2).hide();
$(window).scroll(function () {
    if ($(document).height()-200 <= $(window).scrollTop() + $(window).height()) {
       $('div#graduated div.panel').slice(hides, hides+3).fadeIn('slow');
		hides += 3;
    }
});