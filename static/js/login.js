$(document).ready(function(){
	$("#login").on('click', function(){
		var path = "http://" + window.location.host + "/index.php/authentication/login/";
		$.ajax({
			type: 'POST',
			url: path,
			data: $("#login-form").serialize(),
			success: function(e){
                window.location.reload(true);
			}
		});
		return false;
	});
	$("#logout").on('click', function(){
		var path = "http://" + window.location.host + "/index.php/authentication/logout/";
		$.ajax({
			type: 'POST',
			url: path,
			success: function(){
				window.location.reload(true);
			}
		});
		
		return false;
	});
});