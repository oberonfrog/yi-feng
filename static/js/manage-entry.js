$(document).ready(function(){
	$("button.delete").prop("disabled",false);
	$(document).on('click', '.content-modify', function(){
		$(this).removeClass('content-modify').addClass('content-save').text("儲存");
		$('#input').prop('contenteditable',true).addClass('editable');
		return false;
	});
	$(document).on('click', '.content-save', function(){
		var update = $("#input").html();
		var that = $(this);
		var path = "http://" + window.location.host + "/index.php/edit/introduction/";
		var ajaxdata = {
			'content': update
		};
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(msg){
				that.removeClass('content-save').addClass('content-modify').text("修改");
				$('#input').prop('contenteditable',false).removeClass('editable');
			}
		});
		return false;
	});
	
	$(document).on('keydown', 'div#input', function(e) {
		if (e.keyCode === 13) {
		  pasteHtmlAtCaret('<br>');
		  return false;
		}
	});
	
	$(document).on('click', 'div.latest-news button.modify', function(){
		var focus_tr = $(this).parent().parent();
		var text = $.trim(focus_tr.find('.notice').text());
		var date = $.trim(focus_tr.find('.time').text());
		focus_tr.find('.notice').html("<input class='inputdata notice_data' type='text' value='"+text+"' />");
		focus_tr.find('.time').html("<input class='inputdata datepicker' value='"+ date +"' data-date-format='yyyy-mm-dd'>");
		$(this).removeClass('modify').addClass('save').text("儲存");
		focus_tr.find('.delete').prop('disabled',true);
		$('.datepicker').datepicker();
		
		return false;
	});
	$(document).on('click', 'div.latest-news button.save', function(){
		var focus_tr = $(this).parent().parent();
		var that = $(this);
		var content = $.trim(focus_tr.find('input.notice_data').val());
		var date = $.trim(focus_tr.find('input.datepicker').val());
		var path = "http://" + window.location.host + "/index.php/edit/news/modify/";
		var ajaxdata = {
			'content': content,
			'date': date,
			'sql_id': $(this).attr('data-id')
		};
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(msg){
				//console.log("test");
				focus_tr.find('.notice').text(content);
				focus_tr.find('.time').text(date);
				that.removeClass('save').addClass('modify').text("修改");
				focus_tr.find('.delete').prop('disabled',false);
			}
		});
		return false;
	});
	$(document).on('click', 'div.latest-news button.delete', function(){
		var that = $(this);
		var path = "http://" + window.location.host + "/index.php/edit/news/delete/";
		var ajaxdata = {
			'sql_id': $(this).attr('data-id')
		};
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(){
				that.parent().parent().hide(800, function(){
					$(this).remove();
				});
			}
		});
		return false;
	});
	$(document).on('click', 'div.latest-news button.add', function(){
		$('tbody').prepend('<tr>\
				<td class="notice">\
					<input class="inputdata notice_data" type="text"/></td>\
				<td class="time">\
					<input class="inputdata datepicker" data-date-format="yyyy-mm-dd"></td>\
				<td class="handle">\
					<button type="button" class="additem btn btn-info btn-xs">加入</button>\
				</td>\
				<td class="handle2">\
					<button type="button" class="delitem btn btn-danger btn-xs">取消</button>\
				</td>\
			</tr>');
		$('.datepicker').datepicker();
		return false;
	});
	$(document).on('click', 'div.latest-news button.additem', function(){
		var focus_tr = $(this).parent().parent();
		var content = $.trim(focus_tr.find('input.notice_data').val());
		var that = $(this);
		var date = $.trim(focus_tr.find('input.datepicker').val());
		var path = "http://" + window.location.host + "/index.php/edit/news/add/";
		var ajaxdata = {
			'content': content,
			'date': date
		};
		
		
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(id){
				focus_tr.find('.notice').text(content);
				focus_tr.find('.time').text(date);
				that.removeClass('additem').attr("data-id", $.trim(id)).addClass('modify').text("修改");
				focus_tr.find('.delitem').removeClass('delitem').attr("data-id", $.trim(id)).addClass('delete').text("刪除");
			}
		});
			
		return false;
	});
	$(document).on('click', 'div.latest-news button.delitem', function(){
		$(this).parent().parent().remove();
		return false;
	});
});