$(document).ready(function(){
	$(document).on('keydown', 'div.intro', function(e) {
		if (e.keyCode === 13) {
		  pasteHtmlAtCaret('<br>');
		  return false;
		}
	});
	$(document).on('keydown', 'div.old-member div.panel-body', function(e) {
		if (e.keyCode === 13) {
			pasteHtmlAtCaret('<br>');
			return false;
		}
	});
	$(document).on('click', 'div.lab-member div.add-member button.add', function(){
		var main = $(this).parent();
		var that = $(this);
		var name = main.find('#member-name').val();
		var path = "http://" + window.location.host + "/index.php/edit/member/add";
		var ajaxdata = {
			'name': name
		};
		
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(){
				location.reload(true);
			}
		});
		return false;
	});
	
	$(document).on('click', 'div.lab-member table button.delete', function(){
		var path = "http://" + window.location.host + "/index.php/edit/member/delete";
		var that = $(this);
		var ajaxdata = {
			'sql_id': $(this).attr('data-id')
		};
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(){
				that.parent().parent().parent().parent().parent().hide(1000, function(){
					$(this).remove();
				});
			}
		});
		return false;
	});
	
	$(document).on('click', 'div.old-member button.modify', function(){
		var panel = $(this).parent().parent().parent().parent().parent();
		var paper_name_target = $(this).parent().siblings('div.paper-name');
		var paper_author_target = $(this).parent().siblings('div.paper-author');
		var paper_name = $.trim(paper_name_target.text());
		var paper_author = $.trim(paper_author_target.text());
		
		paper_name_target.html("<input class='inputdata name-data' type='text' value='"+paper_name+"'/>");
		paper_author_target.html("<input class='inputdata author-data' type='text' value='"+paper_author+"'/>");
		panel.find('div.panel-body').addClass('editable').prop('contenteditable', true);
		$(this).removeClass('modify').addClass('save').text("儲存");
		$(this).siblings('.delete').prop("disabled", true);
		return false;
	});
	$(document).on('click', 'div.old-member button.save', function(){
		var panel = $(this).parent().parent().parent().parent().parent();
		var paper_name_target = $(this).parent().siblings('div.paper-name');
		var paper_author_target = $(this).parent().siblings('div.paper-author');
        var paper_cat_target = $(this).parent().siblings("div.paper-cat-tar");
        var paper_author_age = $(this).parent().siblings("div.paper-author-age");
        
		var paper_name = $.trim(paper_name_target.find('input.name-data').val());
		var paper_author = $.trim(paper_author_target.find('input.author-data').val());
        var paper_cat = $.trim(paper_cat_target.find('select').val());
        var author_age = $.trim(paper_author_age.find('input.author-age').val());
		var content = $.trim(panel.find('div.panel-body').html());

		var path = "http://" + window.location.host + "/index.php/edit/old_member/modify";
		var that = $(this);
		var ajaxdata = {
			'name': paper_name,
            'age': author_age,
			'author': paper_author,
            'cat': paper_cat,
			'content': content,
			'sql_id': $(this).attr('data-id')
		};
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(msg){
				console.log(msg);
				paper_name_target.text(paper_name);
                paper_cat_target.text(paper_cat);
                paper_author_age.text(author_age);
				paper_author_target.text(paper_author);
				panel.find('div.panel-body').removeClass('editable').prop('contenteditable', false);
				that.removeClass('save').addClass('modify').text("修改");
				that.siblings('.delete').prop("disabled", false);
			}
		});
		return false;
	});
	$(document).on('click', 'div.old-member button.delete', function(){
		var path = "http://" + window.location.host + "/index.php/edit/old_member/delete";
		var that = $(this);
		var ajaxdata = {
			'sql_id': $(this).attr('data-id')
		};
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(){
				that.parent().parent().parent().parent().parent().parent().hide(1000, function(){
					$(this).remove();
				});
			}
		});
		
	});
	$(document).on('click', 'div.old-member div.add-member button.add', function(){
		var paper_author = $(this).siblings('input#member-name').val();
		var that = $(this);
		var par = $(this).parent().parent();
		var path = "http://" + window.location.host + "/index.php/edit/old_member/add";
		var ajaxdata = {
			'author': paper_author
		};
		$.ajax({
			type: 'POST',
			url: path,
			dataType: "json",
			success: function(id){
				var str = '<div class="panel-group" id="accordion">\
								<div class="panel">\
									<div class="panel-heading">\
										<h4 class="panel-title">\
                                            <div class="row">\
												<div class="col-md-3">論文\
												</div>\
                                                <div class="col-md-3">類別\
												</div>\
                                                <div class="col-md-2">畢業年\
												</div>\
												<div class="col-md-2">作者\
												</div>\
											</div>\
											<div class="row">\
												<div class="col-md-3 paper-name"><input class="inputdata name-data" type="text" />\
												</div>\
                                                <div class="col-md-3 paper-cat-tar"><select class="paper-cat">';
                                                
                                                for(var i = 0; i < id.project.length;i++){
                                                    str = str + "<option value='" + id.project[i].name + "'>" + id.project[i].name + "</option>";
                                                }
                    
                                                
                                                str += '</select>\
												</div>\
                                                <div class="col-md-2 paper-author-age"><input class="inputdata author-age" type="text" />\
												</div>\
												<div class="col-md-2 paper-author"><input class="inputdata author-data" type="text" value="'+paper_author+'"/>\
												</div>\
												<div class="col-md-2">\
													<button type="button" class="save btn btn-info btn-xs" data-id="'+id.sql_id+'">儲存</button>\
													<button type="button" class="delete btn btn-danger btn-xs" data-id="'+id.sql_id+'">刪除</button>\
												</div>\
											</div>\
										</h4>\
									</div>\
									<div class="panel-body editable" contenteditable="true">\
									</div>\
								</div>\
							</div>';
				par.after(str);
			}
		});
		return false;
	});
	
});









