$(document).ready(function(){
	$("button.delete").prop("disabled",false);
  	$(document).on('click', 'div.advisor_exp input[type="checkbox"]', function(){
		if($(this).prop("checked")){
			var dNow = new Date();
			var utcdate = "";
			if(dNow.getMonth() < 10){
				utcdate= dNow.getFullYear() + '-0' + (dNow.getMonth()+1);
			}
			else{
				utcdate= dNow.getFullYear() + '-' + (dNow.getMonth()+1);
			}
		 	$(this).siblings(".end_date_data").val(utcdate).prop("disabled", true);
	   	}
	   	else{
			$(this).siblings(".end_date_data").prop("disabled", false);
	   	}
	});
	$(document).on('click', 'div.advisor_exp button.modify', function(){
		var focus_tr = $(this).parent().parent();
		var institution = $.trim(focus_tr.find('.institution').text());
		var department = $.trim(focus_tr.find('.department').text());
		var job = $.trim(focus_tr.find('.job').text());
		var start_date = $.trim(focus_tr.find('.start_date').text());
		var end_date = $.trim(focus_tr.find('.end_date').text());
		focus_tr.find('.institution').html("<input class='inputdata institution_data' type='text' value='"+institution+"' />");
		focus_tr.find('.department').html("<input class='inputdata department_data' type='text' value='"+department+"' />");
		focus_tr.find('.job').html("<input class='inputdata job_data' type='text' value='"+job+"' />");
		focus_tr.find('.start_date').html("<input class='inputdata datepicker start_date_data' value='"+ start_date +"' data-date-format='yyyy-mm'>");
		focus_tr.find('.end_date').html("<input class='inputdata datepicker end_date_data' value='"+ end_date +"' data-date-format='yyyy-mm'><input type='checkbox' class='now_job'>現職");
		if($(this).hasClass('now')){
			focus_tr.find('.now_job').prop("checked",true);
			focus_tr.find('.end_date_data').prop("disabled", true);
		}
		$(this).removeClass('modify').addClass('save').text("儲存");
		focus_tr.find('.delete').prop('disabled',true);
		$('.datepicker').datepicker();
		
		return false;
	});
	$(document).on('click', 'div.advisor_exp button.save', function(){
		var focus_tr = $(this).parent().parent();
		var that = $(this);
		var institution = $.trim(focus_tr.find('.institution_data').val());
		var department = $.trim(focus_tr.find('.department_data').val());
		var job = $.trim(focus_tr.find('.job_data').val());
		var start_date = $.trim(focus_tr.find('.start_date_data').val());
		var end_date = "";
		if(focus_tr.find('.now_job').prop("checked")){
			end_date = "0000-00";
		}
		else{
		   end_date = $.trim(focus_tr.find('.end_date_data').val());
		}
		var path = "http://" + window.location.host + "/index.php/edit/exp/modify/";
		var ajaxdata = {
			'institution': institution,
			'department': department,
			'job': job,
			'start_date':start_date+"-01",
			'end_date':end_date+"-01",
			'sql_id': $(this).attr('data-id')
		};

		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(){
				//console.log("test");
				focus_tr.find('.institution').text(institution);
				focus_tr.find('.department').text(department);
				focus_tr.find('.job').text(job);
				focus_tr.find('.start_date').text(start_date);
				focus_tr.find('.end_date').text($.trim(focus_tr.find('.end_date_data').val()));
				that.removeClass('save').addClass('modify').text("修改");
				focus_tr.find('.delete').prop('disabled',false);
			}
		});
		return false;
	});
	
	
	$(document).on('click', 'div.advisor_exp button.delete', function(){
		var that = $(this);
		var path = "http://" + window.location.host + "/index.php/edit/exp/delete/";
		var ajaxdata = {
			'sql_id': $(this).attr('data-id')
		};
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(){
				that.parent().parent().hide(800, function(){
					$(this).remove();
				});
			}
		});
		return false;
	});
	$(document).on('click', 'div.advisor_exp button.add', function(){
		$('div.advisor_exp tbody').prepend('<tr>\
				<td class="institution">\
						<input class="inputdata institution_data" type="text" /></td>\
					<td class="department">\
						<input class="inputdata department_data" type="text"></td>\
					<td class="job">\
						<input class="inputdata job_data" type="text"></td>\
					<td class="start_date">\
						<input class="inputdata datepicker start_date_data" data-date-format="yyyy-mm"> </td>\
					<td class="end_date">\
						<input class="inputdata datepicker end_date_data" data-date-format="yyyy-mm"></td>\
					<td class="handle">\
						<button type="button" class="additem btn btn-info btn-xs" data-id="<?php echo $row->sql_id;?>">加入</button>\
					</td>\
					<td class="handle2">\
						<button type="button" class="delitem btn btn-danger btn-xs" data-id="<?php echo $row->sql_id;?>">取消</button>\
					</td>\
			</tr>');
		$('.datepicker').datepicker();
		return false;
	});
	$(document).on('click', 'div.advisor_exp button.additem', function(){
		var focus_tr = $(this).parent().parent();
		var that = $(this);
		var institution = $.trim(focus_tr.find('.institution_data').val());
		var department = $.trim(focus_tr.find('.department_data').val());
		var job = $.trim(focus_tr.find('.job_data').val());
		var start_date = $.trim(focus_tr.find('.start_date_data').val());
		var end_date = "";
		if(focus_tr.find('.now_job').prop("checked")){
			end_date = "0000-00";
		}
		else{
		   end_date = $.trim(focus_tr.find('.end_date_data').val());
		}
		
		var path = "http://" + window.location.host + "/index.php/edit/exp/add/";
		var ajaxdata = {
			'institution': institution,
			'department': department,
			'job': job,
			'start_date':start_date+"-01",
			'end_date':end_date+"-01"
		};
		
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(id){
				focus_tr.find('.institution').text(institution);
				focus_tr.find('.department').text(department);
				focus_tr.find('.job').text(job);
				focus_tr.find('.start_date').text(start_date);
				focus_tr.find('.end_date').text(end_date);
				that.removeClass('additem').attr("data-id", $.trim(id)).addClass('modify').text("修改");
				focus_tr.find('.delitem').removeClass('delitem').attr("data-id", $.trim(id)).addClass('delete').text("刪除");
			}
		});
			
		return false;
	});
	$(document).on('click', 'div.advisor_exp button.delitem', function(){
		$(this).parent().parent().remove();
		return false;
	});
	
	
	
	$(document).on('click', 'div.advisor_speciality button.modify', function(){
		var focus_tr = $(this).parent().parent();
		var speciality = $.trim(focus_tr.find('.speciality').text());
		focus_tr.find('.speciality').html("<input class='inputdata speciality_data' type='text' value='"+speciality+"' />");
		$(this).removeClass('modify').addClass('save').text("儲存");
		focus_tr.find('.delete').prop('disabled',true);
		
		return false;
	});
	$(document).on('click', 'div.advisor_speciality button.save', function(){
		var focus_tr = $(this).parent().parent();
		var that = $(this);
		var speciality = $.trim(focus_tr.find('input.speciality_data').val());
		var path = "http://" + window.location.host + "/index.php/edit/speciality/modify/";
		var ajaxdata = {
			'speciality': speciality,
			'sql_id': $(this).attr('data-id')
		};
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(){
				focus_tr.find('.speciality').text(speciality);
				that.removeClass('save').addClass('modify').text("修改");
				focus_tr.find('.delete').prop('disabled',false);
			}
		});
		return false;
	});
	$(document).on('click', 'div.advisor_speciality button.delete', function(){
		var that = $(this);
		var path = "http://" + window.location.host + "/index.php/edit/speciality/delete/";
		var ajaxdata = {
			'sql_id': $(this).attr('data-id')
		};
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(){
				that.parent().parent().hide(800, function(){
					$(this).remove();
				});
			}
		});
		return false;
	});
	$(document).on('click', 'div.advisor_speciality button.add', function(){
		$('div.advisor_speciality tbody').prepend('<tr>\
				<td class="speciality">\
					<input class="inputdata speciality_data" type="text"/></td>\
				<td class="handle">\
					<button type="button" class="additem btn btn-info btn-xs">加入</button>\
				</td>\
				<td class="handle2">\
					<button type="button" class="delitem btn btn-danger btn-xs">取消</button>\
				</td>\
			</tr>');
		return false;
	});
	$(document).on('click', 'div.advisor_speciality button.additem', function(){
		var focus_tr = $(this).parent().parent();
		var that = $(this);
		var speciality = $.trim(focus_tr.find('input.speciality_data').val());
		var path = "http://" + window.location.host + "/index.php/edit/speciality/add/";
		var ajaxdata = {
			'speciality': speciality
		};

		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(id){
				focus_tr.find('.speciality').text(speciality);
				that.removeClass('additem').attr("data-id", $.trim(id)).addClass('modify').text("修改");
				focus_tr.find('.delitem').removeClass('delitem').attr("data-id", $.trim(id)).addClass('delete').text("刪除");
				
			}
		});
		return false;
	});
	$(document).on('click', 'div.advisor_speciality button.delitem', function(){
		$(this).parent().parent().remove();
		return false;
	});
});