$(document).ready(function(){
	$("#affix-list").affix({
		offset: {
			top: 150,
			bottom: 350
		}
	});
	
	$('#affix-list a').click(function(){
		$('html, body').animate({
			scrollTop: $( $(this).attr('href') ).offset().top
		}, 500);
		return false;
	});
});