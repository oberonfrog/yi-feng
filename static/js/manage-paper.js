$(document).ready(function(){
	$("button.delete").prop("disabled",false);
	$(document).on('keydown', 'div.paper_data', function(e) {
		if (e.keyCode === 13) {
		  pasteHtmlAtCaret('<br>');
		  return false;
		}
	});

	$(document).on('click', 'div.professor-paper button.modify', function(){
		var focus_tr = $(this).parent().parent();
		focus_tr.find('.paper_data').addClass('editable').prop("contenteditable",true);
		$(this).removeClass('modify').addClass('save').text("儲存");
		focus_tr.find('.delete').prop('disabled',true);
		
		return false;
	});
	$(document).on('click', 'div.professor-paper button.save', function(){
		var focus_tr = $(this).parent().parent();
		var that = $(this);
		var paper = $.trim(focus_tr.find('.paper_data').text());
		var path = "http://" + window.location.host + "/index.php/edit/paper/modify/";
		var ajaxdata = {
			'paper': paper,
			'sql_id': $(this).attr('data-id')
		};
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(msg){
				focus_tr.find('.paper_data').removeClass('editable').prop("contenteditable",false);
				that.removeClass('save').addClass('modify').text("修改");
				focus_tr.find('.delete').prop('disabled',false);
			}
		});
		return false;
	});
	$(document).on('click', 'div.professor-paper button.delete', function(){
		var that = $(this);
		var path = "http://" + window.location.host + "/index.php/edit/paper/delete/";
		var ajaxdata = {
			'sql_id': $(this).attr('data-id')
		};
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(){
				that.parent().parent().hide(800, function(){
					$(this).remove();
				});
			}
		});
		return false;
	});
	$(document).on('click', 'div.professor-paper button.add', function(){
		$('tbody').prepend('<tr>\
				<td class="paper">\
					<div class="inputdata paper_data editable" contenteditable="true"></div></td>\
				<td class="handle">\
					<button type="button" class="additem btn btn-info btn-xs">加入</button>\
				</td>\
				<td class="handle2">\
					<button type="button" class="delitem btn btn-danger btn-xs">取消</button>\
				</td>\
			</tr>');
		return false;
	});
	$(document).on('click', 'div.professor-paper button.additem', function(){
		var focus_tr = $(this).parent().parent();
		var paper = $.trim(focus_tr.find('.paper_data').text());
		var that = $(this);
		var path = "http://" + window.location.host + "/index.php/edit/paper/add/";
		var ajaxdata = {
			'paper': paper,
		};
		$.ajax({
			type: 'POST',
			url: path,
			data: ajaxdata,
			success: function(id){
				focus_tr.find('.paper_data').removeClass('editable').prop("contenteditable",false);
				that.removeClass('additem').attr("data-id", $.trim(id)).addClass('modify').text("修改");
				focus_tr.find('.delitem').removeClass('delitem').attr("data-id", $.trim(id)).addClass('delete').text("刪除");
			}
		});
		return false;
	});
	$(document).on('click', 'div.professor-paper button.delitem', function(){
		$(this).parent().parent().remove();
		return false;
	});
});